function vongLap() {
  // Đầu tiên tạo ra một biến chứa giá trị
  var inHTML = "";
  // Dùng vòng lặp for để in số ra màn hình
  for (i = 1; i <= 100; i++) {
    inHTML += i + "-------";
    if (i % 10 == 0) {
      inHTML += `</br>`;
    }
  }
  document.getElementById("ketQuaBai1").innerHTML = `<span>${inHTML}</span>`;
}
vongLap();

// BÀI 2:
function timSoNguyenTo() {
  let arrInput = [];
  document.getElementById("btnBai2").onclick = function () {
    //Input
    let input = document.getElementById("inputBai2").value;
    arrInput.push(input);
    document.getElementById("arrBai2").innerHTML = arrInput;
    document.getElementById("btnKetQuaBai2").onclick = function () {
      arrSoNguyenTo = [];
      arrInput.forEach((item) => {
        let check = true;
        if (item < 2) {
          check = false;
        } else if (item % 2 == 0) {
          check = false;
        } else {
          for (var i = 3; i < Math.sqrt(item); i += 2) {
            if (item % i == 0) {
              check = false;
              break;
            }
          }
        }
        if (check == true) {
          arrSoNguyenTo.push(item);
        }
        document.getElementById("ketQuaBai2").innerHTML = arrSoNguyenTo;
      });
    };
  };
}
timSoNguyenTo();

// Bài 3
function timS() {
  document.getElementById("btnBai3").onclick = function () {
    let n = document.getElementById("inputBai3").value;
    let s = 0;
    let S = 0;
    for (i = 2; i <= n; i++) {
      s = i + s;
    }
    S = s + 2 * n;
    document.getElementById("ketQuaBai3").innerHTML = S;
  };
}
timS();

// Bài 4
function timUocSo() {
  document.getElementById("btnBai4").onclick = function () {
    let n = document.getElementById("inputBai4").value;
    let arr = [];
    for (i = 1; i <= n; i++) {
      if (n % i == 0) {
        arr.push(i);
      }
    }
    document.getElementById("ketQuaBai4").innerHTML = arr;
  };
}
timUocSo();

// Bài 5
function duyetChuoi() {
  document.getElementById("btnBai5").onclick = function () {
    let n = document.getElementById("inputBai5").value;
    let arr = n.split("");
    let arrReverse = [];
    arrReverse = arr.reverse();
    document.getElementById("ketQuaBai5").innerHTML = arrReverse;
  };
}
duyetChuoi();

// Bài 6
function timXLonNhat() {
  let sum = 0;
  for (x = 1; x <= 100; x++) {
    sum += x;
    if (sum > 100) {
      document.getElementById("ketQuaBai6").innerHTML = x;
      break;
    }
  }
}
timXLonNhat();

// Bài 7
function inBangCuuChuong() {
  document.getElementById("btnBai7").onclick = function () {
    let n = document.getElementById("inputBai7").value;
    let content = "";
    for (i = 0; i <= 10; i++) {
      content += `${n} x ${i} = ${n * i}` + `</br>`;
    }
    document.getElementById("ketQuaBai7").innerHTML = content;
  };
}
inBangCuuChuong();

// Bài 8
function chiaBai() {
  document.getElementById("btnBai8").onclick = function () {
    var players = [[], [], [], []];
    var cards = [
      "4K",
      "KH",
      "5C",
      "KA",
      "QH",
      "KD",
      "2H",
      "10S",
      "AS",
      "7H",
      "9K",
      "10D",
    ];
    content= '';
    for (i = 0; i < players.length; i++){
      players[i].push(cards[i]);
    }
    for (i = 0; i < players.length; i++){
      players[i].push(cards[i+4]);
    }
    for (i = 0; i < players.length; i++){
      players[i].push(cards[i+8]);
    }
    for (i = 0; i < players.length; i++){
      content += `
      player${i+1} = ${players[i]} </br>
      `;
    }
    document.getElementById('ketQuaBai8').innerHTML = content;
  };
}
chiaBai();

// Bài 9
function tongSoChoGa (){
  document.getElementById('btnBai9').onclick = function (){
  let m = document.getElementById('inputMBai9').value;
  let n = document.getElementById('inputNBai9').value;
  let dogCount = (n - (m * 2)) / 2;
  let chickenCount = m - dogCount;
  document.getElementById('ketQuaBai9').innerHTML = 'Số lượng chó là: ' + dogCount +'. Số lượng gà là: ' + chickenCount;
  }
}
tongSoChoGa();

// Bài 10
function hour (){
  document.getElementById('btnBai10').onclick = function (){
    let hours  = document.getElementById('inputMBai10').value;
    let minutes  = document.getElementById('inputNBai10').value;
    let hourAngle = (hours*30) + (minutes*0.5);
    let minuteAngle = minutes * 6;
    let angleDifference = hourAngle - minuteAngle
    document.getElementById('ketQuaBai10').innerHTML = angleDifference;
  }
}
hour();
